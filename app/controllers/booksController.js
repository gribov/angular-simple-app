'use strict';

app.controller('BooksController', ['$scope', 'BookFactory', function($scope, bookFactory) {

	$scope.items = bookFactory.getItems();

}]);
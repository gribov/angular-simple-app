'use strict';

app.controller('MainController', ['$scope', '$route', '$routeParams', '$location',
  function MainCtrl($scope, $route, $routeParams, $location) {
    this.$route = $route;
    this.$location = $location;
    this.$routeParams = $routeParams;

    $scope.links = [
        { 'route' : '/', 'title' : 'Home' },
        { 'route' : '/books', 'title' : 'Books' }
    ];
}])
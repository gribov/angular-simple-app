'use strict';

app.controller('BookController', ['$scope', '$routeParams', 'BookFactory', function($scope, $routeParams, BookFactory) {

	let id =  $routeParams.bookId;

	$scope.item = BookFactory.getItem(id);

	console.log($scope.item);

}]);
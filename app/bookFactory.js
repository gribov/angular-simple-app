'use strict';

app.factory('BookFactory', function($http) {
  
  let items = [
		{ id: 1, title: 'Book1', desc : 'Book1 desc'},
		{ id: 2, title: 'Book2', desc : 'Book2 desc'}
  ];

  return {

    getItems : function() {
    	return items;  // http request here  
	},

	getItem : function(id) {
    	return items[id - 1];  // http request here 
	}

  };

})
'use strict';

var app = angular.module('myapp', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'tmpl/home.html',
        controller: 'HomeController',
        controllerAs: 'home'
      })
      .when('/books', {
        templateUrl: 'tmpl/books.html',
        controller: 'BooksController',
        controllerAs: 'books'
      })
      .when('/book/:bookId', {
        templateUrl: 'tmpl/book.html',
        controller: 'BookController',
        controllerAs: 'book'
      });

    $locationProvider.html5Mode(true);
}])
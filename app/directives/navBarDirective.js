'use strict';

app.directive('navBar', function() {
  return {
  	restrict: 'E',
  	templateUrl: 'tmpl/navbar.html',
  	scope: {
      links: '='
    },
  };

});


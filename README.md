## Simple AngularApp

#### Как запустить

1. Установить менеджер bower
2. Файл .bowerrc нужен для того, чтобы запустить bower через прокси.Если прокси нет в вашей сети, то удалите файл.
3. В корневой директории проекта выполнить
4. ``` bower install ```
5. Для работы приложения нужен http сервер (если у вас есть nodeJS и npm, тто можно использовать static $> npm i -g static ; $> static)
6. Корень домена должен совпадать с корневой директорией проекта.
